from flask import Flask, render_template, request, send_file
from wsgiref.simple_server import make_server

import logging
import logging.handlers
import resources

application = Flask(__name__)


@application.route('/', methods=['GET'])
def init():
    return render_template('index.html', text=resources.text)


@application.route('/about', methods=['GET'])
def about():
    return render_template('about.html', text=resources.text)


@application.route('/services', methods=['GET'])
def services():
    return render_template('services.html', text=resources.text)


@application.route('/portfolio', methods=['GET'])
def portfolio():
    return render_template('portfolio.html', text=resources.text)


@application.route('/portfolio/<project_name>', methods=['GET'])
def portfolio_single(project_name):
    return render_template('portfolio-single.html', project=resources.text['projects'][project_name])


@application.route('/contact', methods=['GET'])
def contact():
    return render_template('contact.html', text=resources.text)


if __name__ == "__main__":
    application.run()
