#!/usr/bin/env python
text = {
    'home': {
        'services':
            [
                {
                    'name': 'Pipeline Design',
                    'icon': 'icon-paper-plane',
                    'description': 'We can help you with complicated data pipeline designs.'
                },
                {
                    'name': 'Web Design',
                    'icon': 'icon-screen-desktop',
                    'description': 'We made site templates and change them into dynamic sites with complete functions.'
                },
                {
                    'name': 'Mobile Design',
                    'icon': 'icon-screen-smartphone',
                    'description': 'We make variable mobile applications with different purposes.'
                                   ' Contact us to learn more.'
                },
                {
                    'name': 'Research',
                    'icon': 'icon-magnifier',
                    'description': 'Researching is in our DNA\'s. We do extensive researches '
                                   'before we decide which direction we head to.'
                }
            ]
    },
    'about': {
        'leaders':
            [
                {
                    'name': 'Li Dai',
                    'title': 'Founder',
                    'image': 'li_dai_real.jpg',
                    'link': 'https://www.linkedin.com/in/li-dai-vet/',
                    'description': 'Li is an US Air Force veteran with double bachelors in Psychology, '
                                   'Math, and a master\'s degree in Computer science. He obtained multiple '
                                   'military awards while in the Air Force and had made impact in both Fidelity '
                                   'and White Ops. Li Currently is a solutions architect in Confluent.'
                },
                {
                    'name': 'Eddie Hong',
                    'title': 'Tech Lead',
                    'image': 'eddie_head.jpg',
                    'link': 'https://www.linkedin.com/in/qin-hong-b3353894/',
                    'description': 'ESFE tech lead. Experienced in designing, '
                                   'architecting and supporting enterprise-level internet scale services/solutions.'
                }
            ]
    },
    'services': {
        'services':
            [
                {
                    'icon': 'icon-cursor',
                    'name': 'Research',
                    'description': 'We could help our clients assess their idea validity through in-depth research.'
                },
                {
                    'icon': 'icon-badge',
                    'name': 'Web Design',
                    'description': 'Tired of making websites of template? Want something more unique and stand out? '
                                   'Our design can make customized templates for you.'
                },
                {
                    'icon': 'icon-fire',
                    'name': 'App Design',
                    'description': 'We are experienced in making various types of cross platform apps using both'
                                   ' Flutter and React Native.'
                },
                {
                    'icon': 'icon-layers',
                    'name': 'Relational Design',
                    'description': 'Need help designing highly efficient relational databases? We got it covered.'
                },
                {
                    'icon': 'icon-magnet',
                    'name': 'Game Design',
                    'description': 'ESFE also have side project utilizing Unreal game engine to create a 3D RPG '
                                   'game.'
                },
                {
                    'icon': 'icon-rocket',
                    'name': 'Start Up',
                    'description': 'Would like to work with us and join forces? Reach out to us and see how we '
                                   'can help each other out.'
                }
            ]
    },
    'projects': {
        'contactless':
            {
                'name': 'ESFE Contactless',
                'image': 'contactless/contactless.png',
                'image1': 'contactless/contactless_1.png',
                'image2': 'contactless/contactless_2.jpeg',
                'image3': 'contactless/contactless_3.png',
                'vision': 'Contactless app wants to make local business safer and endure the pandemic. ',
                'type': 'Web',
                'description': 'ESFE contactless is a generic contactless ordering and payment system for vendors to '
                               'avoid physical contact with customers.',
                'link': 'contactless',
                'owners': 'ESFE',
                'contributors': 'All ESFE members',
                'available':
                    {
                        'ios_link': '',
                        'android_link': '',
                        'download_link': '',
                        'visit_link': 'https://contactless.nycesfe.com/'
                    }
            },
        'us_life':
            {
                'name': 'My US Life',
                'image': 'us-life/us-life.PNG',
                'image1': 'us-life/us-life_1.PNG',
                'image2': 'us-life/us-life_2.jpeg',
                'image3': 'us-life/us-life_3.PNG',
                'vision': 'My US Life provides a place for Chinese Americans to share their daily lives. ',
                'type': 'Mobile',
                'description': 'My US Life is a chinese platform for chinese speakers to communicate.',
                'link': 'us_life',
                'owners': 'Tom Lin',
                'contributors': 'Tom Lin, Li Dai',
                'available':
                    {
                        'ios_link': 'https://apps.apple.com/us/app/id1530656253',
                        'android_link': 'https://play.google.com/store/apps/details?id=com.myuslife.apk',
                        'download_link': '',
                        'visit_link': 'http://myuslife.com/'
                    }
            },
        'virus_tracker':
            {
                'name': 'Virus Tracker',
                'image': 'virus_tracker/virus_tracker.jpg',
                'image1': 'virus_tracker/virus_tracker_1.png',
                'image2': 'virus_tracker/virus_tracker_2.png',
                'image3': 'virus_tracker/virus_tracker_3.png',
                'vision': 'Virus Tracker will provide an easy way to keep track of COVID-19. '
                          'It will provide the most updated data as we can find.',
                'type': 'Web',
                'description': 'Virus Tracker is a website that allows users'
                               ' to find the latest COVID-19 info worldwide. ',
                'link': 'virus_tracker',
                'owners': 'Jianhui Lin',
                'contributors': 'Jianhui Lin, Shuwen Wang, Li Dai',
                'available':
                    {
                        'ios_link': '',
                        'android_link': '',
                        'download_link': '',
                        'visit_link': 'https://covid19.nycesfe.com/'
                    }
            },
        'project_hermes':
            {
                'name': 'Project Hermes',
                'image': 'hermes/hermes.png',
                'image1': 'work_1.jpg',
                'image2': 'work_2.jpg',
                'image3': 'work_3.jpg',
                'vision': 'Project Hermes aims to free people fingers and allow them to '
                          'order groceries through IoT',
                'type': 'IoT',
                'description': 'Hermes was a DevPost Hackathon project. We used Java Quarkus '
                               'framework to build an Alexa skill to order deliveries through '
                               'Amazon Echo.',
                'link': 'project_hermes',
                'owners': 'Li Dai',
                'contributors': 'Jianhui Lin, Shuwen Wang, Li Dai',
                'available':
                    {
                        'ios_link': '',
                        'android_link': '',
                        'download_link': 'https://github.com/ldai100/Quarkus-Alexa-Hackathon',
                        'visit_link': 'https://amzn.to/3hn3tF8'
                    }
            },
        'dashboard':
            {
                'name': 'NYC Dashboard',
                'image': 'dashboard/dashboard.jpg',
                'image1': 'dashboard/dashboard_1.png',
                'image2': 'dashboard/dashboard_2.png',
                'image3': 'dashboard/dashboard_3.png',
                'vision': 'New Yorker will be using our app daily for all commute related information. '
                          'The dashboard will collect the most important info/metric per the user. '
                          'It should consist everything they need for daily dash.',
                'type': 'Mobile',
                'description': 'NYC Dashboard provides bus information according to your geo-location. '
                               'On top of that, it tells you the weather, subway status, and public services info. '
                               'It is meant to provide everything you need in a morning dash.',
                'link': 'dashboard',
                'owners': 'Tom Lin',
                'contributors': 'Tom Lin, Jianhui Lin, Howard Loh, Li Dai',
                'available':
                    {
                        'ios_link': '',
                        'android_link': 'https://play.google.com/store/apps/details?id=com.esfe.NYCDashboard',
                        'download_link': '',
                        'visit_link': ''
                    }
            },
        'swordsman':
            {
                'name': 'Swordsman',
                'image': 'swordsman/swordsman.jpg',
                'image1': 'swordsman/swordsman_1.png',
                'image2': 'swordsman/swordsman_2.png',
                'image3': 'swordsman/swordsman_3.png',
                'vision': 'Swordsman is meant for us to explore Unreal engine and its functions. '
                          'On top of that, we aim to make it a conventional RPG that passes stages as player goes on.',
                'type': 'Game',
                'description': 'For now, it is a random RPG game.',
                'link': 'swordsman',
                'owners': 'Sam Feng',
                'contributors': 'Sam Feng',
                'available':
                    {
                        'ios_link': '',
                        'android_link': '',
                        'download_link': 'https://esfe.itch.io/swordsman',
                        'visit_link': ''
                    }
            },
        'un_choo':
            {
                'name': 'Un-Choo',
                'image': 'work_2.jpg',
                'image1': 'work_3.jpg',
                'image2': 'work_4.jpg',
                'image3': 'work_1.jpg',
                'vision': 'People don\'t know what they want. Un-Choo is meant to find best places/venues user will be '
                          'interested on-demand.',
                'type': 'Mobile',
                'description': '',
                'link': 'un_choo',
                'owners': 'Jianhui Lin',
                'contributors': 'Jianhui Lin, Li Dai',
                'available':
                    {
                        'ios_link': '',
                        'android_link': '',
                        'download_link': '',
                        'visit_link': ''
                    }
            },
        'templates':
            {
                'name': 'ESFE Templates',
                'image': 'work_4.jpg',
                'image1': 'work_1.jpg',
                'image2': 'work_2.jpg',
                'image3': 'work_3.jpg',
                'vision': '',
                'type': 'Design',
                'description': '',
                'link': 'templates',
                'owners': 'Robby Zhang',
                'contributors': 'Robby Zhang, Li Dai',
                'available':
                    {
                        'ios_link': '',
                        'android_link': '',
                        'download_link': '',
                        'visit_link': ''
                    }
            },
        'baymax':
            {
                'name': 'Baymax',
                'image': 'baymax/baymax.jpg',
                'image1': 'baymax/baymax_1.png',
                'image2': 'baymax/baymax_2.png',
                'image3': 'baymax/baymax_3.png',
                'vision': 'As of Now, Project Baymax has been terminated indefinitely.',
                'type': 'Mobile',
                'description': 'Baymax is a mobile app to help working mother/father to easily find care givers. '
                               'It also intends to provide extra income opportunities for stay home mom/dad.',
                'link': 'baymax',
                'owners': 'Li Dai',
                'contributors': 'Li Dai, Yaofeng Chen, Tom Lin, Jianhui Lin, Howard Loh, Alex Wei, '
                                'Robby Zhang, Junbing Liang',
                'available':
                    {
                        'ios_link': '',
                        'android_link': '',
                        'download_link': '',
                        'visit_link': ''
                    }
            }
    },
    'contact': {

    },
    'team': [
        {
            'name': 'Li Dai',
            'title': 'Founder & Product Manager',
            'image': 'Li_Dai.jpeg',
            'description': 'Li is an US Air Force veteran with double bachelors in Psychology, '
                           'Math, and a master\'s degree in Computer science. '
                           'He worked in both Fidelity and White Ops as a Engineer.'
        },
        {
            'name': 'Youjie Lin',
            'title': 'Software Engineer',
            'image': 'tom.jpg',
            'description': 'Professional full stack software developer, '
                           'self-motivated learners with a strong IT background skill.'
        },
        {
            'name': 'Shuwen Wang',
            'title': 'Software Engineer',
            'image': 'shuwen.jpeg',
            'description': 'Graduated from University Of Washington with a mathematics degree. '
                           'A passionate Full Stack Developer who enjoys learning '
                           'new technology and algorithms in computer science.'
        },
        {
            'name': 'Jianhui Lin',
            'title': 'Full Stack Software Engineer',
            'image': 'jianhui.jpg',
            'description': 'Jianhui is passionate and motivated Full Stack Engineer who has '
                           'strong foundation in Algorithm and Critical Thinking. '
                           'He\'s a quick learner and enjoys take on challenges.'
        },
        {
            'name': 'Howard Loh',
            'title': 'Software Engineer',
            'image': 'howard.png',
            'description': 'Motivated Software Developer who welcomes challenging projects '
                           'and enjoys working with all sorts of personalities. '
                           'Currently Computer Science student at Queens College.'
        },
        {
            'name': 'Jiewen Ying',
            'title': 'Software Engineer',
            'image': 'jiewen.jpg',
            'description': 'Graduated from Queens College with a bachelor degree in Computer Science. '
                           'Software Developer who love learning new skills and taking challenges.'
        },
        {
            'name': 'Shangshang Zhu',
            'title': 'Software Engineer',
            'image': 'shangshang.jpg',
            'description': 'A Recent Computer Science graduate specializing in full software development life cycle, '
                           'gathering requirement specifications and user documentation'
        },
        {
            'name': 'Eddie Hong',
            'title': 'Tech Lead',
            'image': 'eddie.png',
            'description': 'ESFE tech lead. Experienced in designing, '
                           'architecting and supporting enterprise-level internet scale services/solutions.'
        }
    ]
}
